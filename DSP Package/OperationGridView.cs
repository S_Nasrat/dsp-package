﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DSP_Package
{
    public partial class OperationGridView : Form
    {
        bool isFormClosed;
        Signal s;
        int numOfBits;
        List<double> yCoordsbeforequant = new List<double>();
        List<SignalProcessing.Interval> L = new List<SignalProcessing.Interval>();
        public OperationGridView()
        {
            InitializeComponent();
            isFormClosed = false;
        }

        private void OperationGridView_Load(object sender, EventArgs e)
        {

        }

        private void closeformBT_Click(object sender, EventArgs e)
        {
            dataGridView1.ColumnCount = 6;
            dataGridView1.RowCount = s.getYCoords().Count+1;
            dataGridView1[0, 0].Value = "Point #";
            dataGridView1[1, 0].Value = "Actual Point";
            dataGridView1[2, 0].Value = "Interval Index";
            dataGridView1[3, 0].Value = "Interval Coding";
            dataGridView1[4, 0].Value = "New Mapped Point";
            dataGridView1[5, 0].Value = "Error M.P - A. P";
            //Point #
            for (int i = 1; i <= s.getYCoords().Count; i++)
            {
                dataGridView1[0, i].Value = i - 1;
            }
            //Actual P
            for (int i = 1; i <= s.getYCoords().Count; i++)
            {
                dataGridView1[1, i].Value = yCoordsbeforequant[i-1];
            }
            //Interval Index and Interval Coding
            for (int i = 1; i <= s.getYCoords().Count; i++)
            {
                for (int j = 0; j < L.Count; j++)
                {
                    if (s.getYCoords()[i - 1] == L[j].GetMidpoint())
                    {
                        dataGridView1[2, i].Value = j;
                        dataGridView1[3, i].Value = Convert.ToString(j, 2).PadLeft(numOfBits, '0');
                        break;
                    }
                }
            }
            //New Mapped Point
            for (int i = 1; i <= s.getYCoords().Count; i++)
            {
                dataGridView1[4, i].Value = s.getYCoords()[i - 1];
            }
            //Error
            for (int i = 1; i <= s.getYCoords().Count; i++)
            {
                dataGridView1[5, i].Value = Convert.ToDouble(dataGridView1[4, i].Value) - Convert.ToDouble(dataGridView1[1, i].Value);
            }
            //Mean Square Error
            
        }
        public void SetSignal(Signal newS)
        {
            this.s = new Signal(newS.getYCoords()) ;
        }
        public void SetIntervalList(List<SignalProcessing.Interval> newL)
        {
            this.L = new List<SignalProcessing.Interval>(newL);
        }
        public void SetYCoordBeforeQuant(List<double> L)
        {
            yCoordsbeforequant = new List<double>(L);
        }
        public void SetNumberOfBits(int n)
        {
            numOfBits = n;
        }
        
    }
}
