﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DSP_Package
{
    public class InputReader
    {
        public static List<double> readSignal(string filePath)
        {
            List<double> yCoords = new List<double>();
            string s;
            StreamReader file = new StreamReader(filePath);
            while ((s = file.ReadLine()) != null)
            {
                yCoords.Add(double.Parse(s));
            }
            return yCoords;
        }
        public static void SaveSignal(Signal S)
        {
            SaveFileDialog sdia = new SaveFileDialog();
            sdia.Title = "Save Signal";
            sdia.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (sdia.ShowDialog() == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(sdia.FileName);
                foreach (double d in S.getYCoords())
                {
                    sw.WriteLine(d);
                }
                sw.Close();
            }
            
        }
        public static void SaveSignal(List<double> l)
        {
            SaveFileDialog sdia = new SaveFileDialog();
            sdia.Title = "Save Signal";
            sdia.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (sdia.ShowDialog() == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(sdia.FileName);
                foreach (double d in l)
                {
                    sw.WriteLine(d);
                }
                sw.Close();
            }

        }
        public static void SaveSignal(List<double> amplitude , List<double> phase)
        {
            SaveFileDialog sdia = new SaveFileDialog();
            sdia.Title = "Save Frequency Components";
            sdia.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (sdia.ShowDialog() == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(sdia.FileName);
                for (int i = 0; i < amplitude.Count;i++)
                {
                    string temp = phase[i].ToString("R");
                    sw.WriteLine(amplitude[i] + "\t\t" + phase[i]);
                }
                sw.Close();
            }

        }
        public static List<ComplexNumber> ReadPolar()
        {
            List<double> Amp = new List<double>();
            List<double> Phase = new List<double>();
            List<ComplexNumber> L = new List<ComplexNumber>();
             OpenFileDialog f1 = new OpenFileDialog();
           
                f1.Title = "Select the signal data";
           

                if (f1.ShowDialog() == DialogResult.OK)
                {
                    string s;
                    StreamReader file = new StreamReader(f1.FileName);
                    while ((s = file.ReadLine()) != null)
                    {
                        string[] lol = s.Split('\t');
                        Amp.Add(double.Parse(lol[0]));
                        Phase.Add(double.Parse(lol[1]));
                        
                    }
                    for(int i=0;i<Amp.Count;i++)
                    {
                        ComplexNumber c = new ComplexNumber();
                        c.SetAmp(Amp[i]);
                        c.SetPhase(Phase[i]);
                        c.PolarToComplex();
                        L.Add(c);
                    }
                }
                return L;
        }
        public static Signal ReadXYSignal()
        {
            List<double> xcoords = new List<double>();
            List<double> ycoords = new List<double>();
            OpenFileDialog f1 = new OpenFileDialog();

            f1.Title = "Select the signal data";


            if (f1.ShowDialog() == DialogResult.OK)
            {
                string s;
                StreamReader file = new StreamReader(f1.FileName);
                while ((s = file.ReadLine()) != null)
                {
                    string[] lol = s.Split('\t');
                    xcoords.Add(double.Parse(lol[0]));
                    ycoords.Add(double.Parse(lol[1]));

                }
            }
            Signal newSignal = new Signal(xcoords, ycoords);

            return newSignal;
        }

    }
}
