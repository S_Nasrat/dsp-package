﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZedGraph;
namespace DSP_Package
{
    public class DrawSignals
    {
       
        public static void DrawSignal(Signal S, ref ZedGraphControl g1,System.Drawing.Color signalColor,string title)
        {
           
           

            PointPairList s1Points = new PointPairList();
            s1Points.Clear();
            //signal1GraphPane.Title = "Signal(s) Plotting";
            //signal1GraphPane.XAxis.Title = "Time";
            //signal1GraphPane.YAxis.Title = "Voltage";
            g1.GraphPane.XAxis.Title.Text = "Time";
            g1.GraphPane.YAxis.Title.Text = "Voltage";
            g1.GraphPane.Title.Text = "Signal(s) Plotting";
            
            List<double> yTemp = new List<double>(S.getYCoords());
            for (int i = 0; i < S.getYCoords().Count; i++)
            {
                s1Points.Add(new PointPair(i, S.getYCoords()[i]));
            }
            LineItem lt1 = g1.GraphPane.AddCurve(title, s1Points, signalColor, SymbolType.Circle);
            lt1.Line.IsVisible = false;
            g1.AxisChange();
            g1.Invalidate();
        }
        public static void DrawSignal(List<double> xaxis,List<double> yaxis, ref ZedGraphControl g1, System.Drawing.Color signalColor, string yaxisTitle)
        {



            PointPairList s1Points = new PointPairList();
            s1Points.Clear();
           
            g1.GraphPane.XAxis.Title.Text = "Frequency";
            g1.GraphPane.YAxis.Title.Text = yaxisTitle;
            g1.GraphPane.Title.Text = yaxisTitle +" and Frequency Plotting";

            
            for (int i = 0; i < xaxis.Count; i++)
            {
                s1Points.Add(new PointPair(xaxis[i], yaxis[i]));
            }
            LineItem lt1 = g1.GraphPane.AddCurve("", s1Points, signalColor, SymbolType.Circle);
            lt1.Line.IsVisible = false;
            g1.AxisChange();
            g1.Invalidate();
        }
        public static void DrawSignalXY(List<double> xaxis, List<double> yaxis, ref ZedGraphControl g1, System.Drawing.Color signalColor,string signalname)
        {



            PointPairList s1Points = new PointPairList();
            s1Points.Clear();

            g1.GraphPane.XAxis.Title.Text = "X-Axis";
            g1.GraphPane.YAxis.Title.Text = "Y-Axis";
            g1.GraphPane.Title.Text = signalname;


            for (int i = 0; i < xaxis.Count; i++)
            {
                s1Points.Add(new PointPair(xaxis[i], yaxis[i]));
            }
            LineItem lt1 = g1.GraphPane.AddCurve("", s1Points, signalColor, SymbolType.Circle);
            lt1.Line.IsVisible = false;
            g1.AxisChange();
            g1.Invalidate();
        }

        public static void ClearGraph(ZedGraphControl g1)
        {

            g1.GraphPane.CurveList.Clear();
            g1.GraphPane.GraphObjList.Clear();
            g1.Invalidate();
        }

       

    }
}
