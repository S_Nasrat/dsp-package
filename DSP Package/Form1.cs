﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace DSP_Package
{
    public partial class Form1 : Form
    {
       
          List<double> temp = new List<double>();
          Signal s1;
          Signal s2;
          Signal TSignal;
          /*  Signal S4 = new Signal();*/
        OperationGridView grid = new OperationGridView();
        public Form1()
        {
            InitializeComponent();         

        }

        private void browseTB_Click(object sender, EventArgs e)
        {
            DrawSignals.ClearGraph(signal1ZGraph);
            if (!onesignalRB.Checked && !twosignalsRB.Checked)
            {
                MessageBox.Show("You must first select signal mode!");
                return;
            }
            OpenFileDialog f1 = new OpenFileDialog();
            if (onesignalRB.Checked)
            {
               
                f1.Title = "Select the signal data";
            }
            else
            {
               

                f1.Title = "Select the data for both signals";
                f1.Multiselect = true;

            }
            browseTB.Text = f1.FileName;
            if (f1.ShowDialog() == DialogResult.OK)
            {
                if (onesignalRB.Checked)
                {
                    
                    s1 = new Signal();
                   
                        s1.SetSignal(f1.FileName);
                    
                  DrawSignals.DrawSignal(s1,ref  signal1ZGraph,Color.HotPink, Path.GetFileNameWithoutExtension(f1.FileName));
                  if (applydftCB.Checked)
                  {
                      SignalProcessing.DFT(s1, Convert.ToDouble(samplefTB.Text), ref ampfreqZGraph, ref phasefreqZGraph);
                  }
                }
                else
                {
                    s1 = new Signal();
                    s2 = new Signal();
                    s1.SetSignal(f1.FileNames[0]);
                    s2.SetSignal(f1.FileNames[1]);
                    DrawSignals.DrawSignal(s1, ref signal1ZGraph, Color.HotPink, Path.GetFileNameWithoutExtension(f1.FileNames[0]));
                    DrawSignals.DrawSignal(s2, ref  signal1ZGraph, Color.DarkCyan, Path.GetFileNameWithoutExtension(f1.FileNames[1]));
                    DialogResult dr = MessageBox.Show("Would you like to add the 2 signals ?", "Addition", MessageBoxButtons.YesNo);
                    {
                        if(dr == DialogResult.Yes)
                        {
                            Signal resultantSignal = SignalProcessing.AddSignals(s1, s2);
                            MessageBox.Show("Select the destination of the resultant signal");
                            InputReader.SaveSignal(resultantSignal);
                            DrawSignals.ClearGraph(signal1ZGraph);
                            DrawSignals.DrawSignal(resultantSignal, ref signal1ZGraph, Color.RoyalBlue,"Resultant Signal");
                            if (applydftCB.Checked)
                            {
                                SignalProcessing.DFT(resultantSignal, Convert.ToDouble(samplefTB.Text), ref ampfreqZGraph, ref phasefreqZGraph);
                            }
                        }
                        else
                        {
                            return;
                        }
                    }
                    

                }


            }

        }



        private void button1_Click(object sender, EventArgs e)
        {
            DrawSignals.ClearGraph(signal1ZGraph);
        }

        private void addBT_Click(object sender, EventArgs e)
        {
           
        }

        private void quantizeBT_Click(object sender, EventArgs e)
        {
            DrawSignals.ClearGraph(signal1ZGraph);
            List<SignalProcessing.Interval> tempInterv = new List<SignalProcessing.Interval>();

            if (!bitsmodeRB.Checked && !levelsmodeRB.Checked)
            {
                MessageBox.Show("You must first select quantization mode!");
                return;
            }
            Signal s = new Signal();
            DrawSignals.ClearGraph(signal1ZGraph);
          
            OpenFileDialog f1 = new OpenFileDialog();
           
                f1.Title = "Select the signal data";


                if (f1.ShowDialog() == DialogResult.OK)
                {
                   
                    if (bitsmodeRB.Checked)
                    {
                        s.SetSignal(f1.FileName);
                        grid.SetYCoordBeforeQuant(s.getYCoords());
                     tempInterv = SignalProcessing.QuantizeSignalWithBits(ref s, Convert.ToInt32(quantmodeTB.Text));
                     grid.SetNumberOfBits(Convert.ToInt32(quantmodeTB.Text));
                     DrawSignals.DrawSignal(s, ref signal1ZGraph, Color.DarkCyan, Path.GetFileNameWithoutExtension(f1.FileName));
                    }
                    else
                    {
                        s.SetSignal(f1.FileName);
                        grid.SetYCoordBeforeQuant(s.getYCoords());
                       tempInterv = SignalProcessing.QuantizeSignalWithLevels(ref s, Convert.ToInt32(quantmodeTB.Text));
                       double n = Math.Log(Convert.ToInt32(quantmodeTB.Text));
                       n = Math.Ceiling(n);
                       grid.SetNumberOfBits(Convert.ToInt32(n));
                       DrawSignals.DrawSignal(s, ref signal1ZGraph, Color.Turquoise, Path.GetFileNameWithoutExtension(f1.FileName));
                    }
                }
                grid.SetIntervalList(tempInterv);
                grid.SetSignal(s);
                grid.Show();
        }

        private void viewdatagridBT_Click(object sender, EventArgs e)
        {

        }

        private void signal1ZGraph_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
          
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void idftBT_Click(object sender, EventArgs e)
        {
             //OpenFileDialog f1 = new OpenFileDialog();
           
             //   f1.Title = "Select the frequency components data";


             //   if (f1.ShowDialog() == DialogResult.OK)
             //   {
             //       //List <ComplexNumbers> temp = InputReader.function()  , function bta5od string path(f1.FileName) w terga3 list of ComplexNumbers
             //       //Signal lolz = new Signal();
             //       //lolz = SignalProcessing.IDFT(temp);

             //   }
           

            //c.Add(new ComplexNumber(64, 0));
            //c.Add(new ComplexNumber(-8, 19.3137084989848));
            //c.Add(new ComplexNumber(-8, 8));
            //c.Add(new ComplexNumber(-8, 3.31370849898476));
            //c.Add(new ComplexNumber(-8, -3.31370849898476));
            //c.Add(new ComplexNumber(-8, -8));
            //c.Add(new ComplexNumber(-8, -19.3137084989848));
           List<ComplexNumber> L =  InputReader.ReadPolar();
           Signal IDFT_S = new Signal(SignalProcessing.IDFT(L));


        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            List<ComplexNumber> l = new List<ComplexNumber>();
            List<ComplexNumber> lr = new List<ComplexNumber>();

            l.Add(new ComplexNumber(1, 0));
            l.Add(new ComplexNumber(3, 0));
            l.Add(new ComplexNumber(5, 0));
            l.Add(new ComplexNumber(7, 0));
            l.Add(new ComplexNumber(9, 0));
            l.Add(new ComplexNumber(11, 0));
            l.Add(new ComplexNumber(13, 0));
            l.Add(new ComplexNumber(15, 0));

            //l.Add(new ComplexNumber(0, 0));
            //l.Add(new ComplexNumber(1, 0));
            //l.Add(new ComplexNumber(2, 0));
            //l.Add(new ComplexNumber(3, 0));


            lr = new List<ComplexNumber>(SignalProcessing.FFT(l, l.Count));


        }

        private void foldBT_Click(object sender, EventArgs e)
        {
           
           
        }

        private void shiftBT_Click(object sender, EventArgs e)
        {
           
            DrawSignals.ClearGraph(signal1ZGraph);
            //Signal shiftedSignal = new Signal();
            s1 = SignalProcessing.ShiftSignal(s1,Convert.ToInt32(shftTB.Text));
            DrawSignals.DrawSignalXY(s1.getXCoords(), s1.getYCoords(), ref signal1ZGraph, Color.HotPink, "Shifted Signal");
        }

        private void foldBT_Click_1(object sender, EventArgs e)
        {
           
            DrawSignals.ClearGraph(signal1ZGraph);
           // Signal foldedSignal = new Signal();
            s1 = SignalProcessing.FoldSignal(s1);
            DrawSignals.DrawSignalXY(s1.getXCoords(), s1.getYCoords(), ref signal1ZGraph, Color.HotPink, "Folded Signal");
        }

       

        private void multiplyBT_Click(object sender, EventArgs e)
        {
            Signal resultantSignal = SignalProcessing.MultiplySignals(s1, s2);
            InputReader.SaveSignal(resultantSignal);
            DrawSignals.ClearGraph(signal1ZGraph);
            DrawSignals.DrawSignal(resultantSignal, ref signal1ZGraph, Color.RoyalBlue, "Resultant Signal");
        }

        private void multiplyconstBT_Click(object sender, EventArgs e)
        {
            double mConst = Convert.ToDouble(multiplyconstTB.Text);
            Signal resultantSignal = SignalProcessing.MultiplySignalByConst(s1, mConst);
            InputReader.SaveSignal(resultantSignal);
            DrawSignals.ClearGraph(signal1ZGraph);
            DrawSignals.DrawSignal(resultantSignal, ref signal1ZGraph, Color.RoyalBlue, "Resultant Signal");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            s1 = InputReader.ReadXYSignal();
            DrawSignals.DrawSignalXY(s1.getXCoords(), s1.getYCoords(), ref signal1ZGraph, Color.HotPink, "S1");
        }

        private void convBT_Click(object sender, EventArgs e)
        {
            Signal convResult = new Signal();
            if (fastconvRB.Checked)
            {
                convResult = SignalProcessing.FastCorrelation(s1, s2, SignalProcessing.SignalMode.fastconvolution);
            }
            else if (normalconvRB.Checked)
            {
                 convResult = SignalProcessing.ConvolutionSum(s1, s2); //change to s2 , s1
            }
           
            DrawSignals.ClearGraph(signal1ZGraph);
            DrawSignals.DrawSignal(convResult, ref  signal1ZGraph, Color.HotPink, "Convolution Result");
            InputReader.SaveSignal(convResult);


        }

        private void directcorrelationBT_Click(object sender, EventArgs e)
        {
            Signal correlationResult = new Signal();
            if(!fftRB.Checked && !directRB.Checked)
            {
                MessageBox.Show("You must first select the Auto Correlation Mode");
                return;
            }
            if (fftRB.Checked)
            {
                correlationResult = SignalProcessing.FastCorrelation(s1, s1, SignalProcessing.SignalMode.none);
            }
            else if (directRB.Checked)
            {

                if (periodicCB.Checked)
                {
                    correlationResult = SignalProcessing.DirectCorrelation(s1,s1,SignalProcessing.SignalMode.periodic);
                }
                else if (nonperiodicCB.Checked)
                {
                    correlationResult = SignalProcessing.DirectCorrelation(s1, s1, SignalProcessing.SignalMode.nonperiodic);
                }
              
            }
                DrawSignals.ClearGraph(signal1ZGraph);
                DrawSignals.DrawSignal(correlationResult, ref  signal1ZGraph, Color.HotPink, "Auto Correlation Result");
                InputReader.SaveSignal(correlationResult);

          
        }

        private void button3_Click(object sender, EventArgs e)
        {


            Signal correlationResult = new Signal();
             if(!crossDirectRB.Checked && !crossFFTRB.Checked)
            {
                MessageBox.Show("You must first select the Cross Correlation Mode");
                return;
            }
            if (crossFFTRB.Checked)
            {
                correlationResult = SignalProcessing.FastCorrelation(s1, s2, SignalProcessing.SignalMode.none);

            }
            else if (crossDirectRB.Checked)
            {

               
                
                    if (periodicCB.Checked)
                    {
                        correlationResult = SignalProcessing.DirectCorrelation(s1, s2, SignalProcessing.SignalMode.periodic);
                    }
                    else if (nonperiodicCB.Checked)
                    {
                        correlationResult = SignalProcessing.DirectCorrelation(s1, s2, SignalProcessing.SignalMode.nonperiodic);
                    }
                
               
               
            }
            DrawSignals.ClearGraph(signal1ZGraph);
            DrawSignals.DrawSignal(correlationResult, ref  signal1ZGraph, Color.HotPink, "Cross Correlation Result");
            InputReader.SaveSignal(correlationResult);
        }

        private void ifftBT_Click(object sender, EventArgs e)
        {
            List<ComplexNumber> mylist = new List<ComplexNumber>();
            mylist.Add(new ComplexNumber(4,0));
            mylist.Add(new ComplexNumber(2, 0));
            mylist.Add(new ComplexNumber(0, 0));
            mylist.Add(new ComplexNumber(2, 0));
            mylist = new List<ComplexNumber>(SignalProcessing.IFFT(mylist,mylist.Count));

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Signal result = new Signal();
            //hiding pass2 incase of low and high
            if (noSampingCB.Checked)
            {
                if (filterCB.SelectedItem == "Low Pass" || filterCB.SelectedItem == "High Pass")
                {
                    pass1LB.Hide();
                    pass1TB.Hide();
                    pass2LB.Text = "Passband";
                    double passband = Convert.ToDouble(pass2TB.Text);
                    double trans_width = Convert.ToDouble(twTB.Text);
                    double stop_att = Convert.ToDouble(stTB.Text);
                    double sample_freq = Convert.ToDouble(sfTB.Text);

                    if (filterCB.SelectedItem == "Low Pass")
                        result = SignalProcessing.Filter(s1, SignalProcessing.FilterMode.lowpass, passband, 0, trans_width, stop_att, sample_freq);
                    else
                        result = SignalProcessing.Filter(s1, SignalProcessing.FilterMode.highpass, passband, 0, trans_width, stop_att, sample_freq);



                }
                else
                {
                    pass2LB.Text = "Passband2";
                    double passband1 = Convert.ToDouble(pass1TB.Text);
                    double passband2 = Convert.ToDouble(pass2TB.Text);

                    double trans_width = Convert.ToDouble(twTB.Text);
                    double stop_att = Convert.ToDouble(stTB.Text);
                    double sample_freq = Convert.ToDouble(sfTB.Text);
                    if (filterCB.SelectedItem == "Band Pass")
                        result = SignalProcessing.Filter(s1, SignalProcessing.FilterMode.bandpass, passband1, passband2, trans_width, stop_att, sample_freq);
                    else
                        result = SignalProcessing.Filter(s1, SignalProcessing.FilterMode.bandstop, passband1, passband2, trans_width, stop_att, sample_freq);
                }
            }
            else
            {
                if(Up_sampling_CB.Checked)
                {
                     double passband = Convert.ToDouble(pass2TB.Text);
                    double trans_width = Convert.ToDouble(twTB.Text);
                    double stop_att = Convert.ToDouble(stTB.Text);
                    double sample_freq = Convert.ToDouble(sfTB.Text);
                     int L = Convert.ToInt32(L_TB.Text);
                     if (filterCB.SelectedItem == "Low Pass")
                     result = SignalProcessing.Sampling(s1,SignalProcessing.SamplingMode.upsampling,L,0,SignalProcessing.FilterMode.lowpass,passband,0,trans_width,stop_att,sample_freq);
                     else if(filterCB.SelectedItem == "High Pass")
                         result = SignalProcessing.Sampling(s1, SignalProcessing.SamplingMode.upsampling, L, 0, SignalProcessing.FilterMode.highpass, passband, 0, trans_width, stop_att, sample_freq);
                     else if (filterCB.SelectedItem == "Band Pass")
                     {
                         double passband1 = Convert.ToDouble(pass1TB.Text);
                         double passband2 = Convert.ToDouble(pass2TB.Text);
                         result = SignalProcessing.Sampling(s1, SignalProcessing.SamplingMode.upsampling, L, 0, SignalProcessing.FilterMode.bandpass, passband1, passband2, trans_width, stop_att, sample_freq);
                     }
                     else
                     {
                         double passband1 = Convert.ToDouble(pass1TB.Text);
                         double passband2 = Convert.ToDouble(pass2TB.Text);
                         result = SignalProcessing.Sampling(s1, SignalProcessing.SamplingMode.upsampling, L, 0, SignalProcessing.FilterMode.bandstop, passband1, passband2, trans_width, stop_att, sample_freq);
                     }
                }
                else if(Down_sampling_CB.Checked)
                {
                    double passband = Convert.ToDouble(pass2TB.Text);
                    double trans_width = Convert.ToDouble(twTB.Text);
                    double stop_att = Convert.ToDouble(stTB.Text);
                    double sample_freq = Convert.ToDouble(sfTB.Text);
                    int M = Convert.ToInt32(M_TB.Text);
                    if (filterCB.SelectedItem == "Low Pass")
                        result = SignalProcessing.Sampling(s1, SignalProcessing.SamplingMode.downsampling, 0, M, SignalProcessing.FilterMode.lowpass, passband, 0, trans_width, stop_att, sample_freq);
                    else if (filterCB.SelectedItem == "High Pass")
                        result = SignalProcessing.Sampling(s1, SignalProcessing.SamplingMode.downsampling, 0, M, SignalProcessing.FilterMode.highpass, passband, 0, trans_width, stop_att, sample_freq);
                    else if (filterCB.SelectedItem == "Band Pass")
                    {
                        double passband1 = Convert.ToDouble(pass1TB.Text);
                        double passband2 = Convert.ToDouble(pass2TB.Text);
                        result = SignalProcessing.Sampling(s1, SignalProcessing.SamplingMode.downsampling, 0, M, SignalProcessing.FilterMode.bandpass, passband1, passband2, trans_width, stop_att, sample_freq);
                    }
                    else
                    {
                        double passband1 = Convert.ToDouble(pass1TB.Text);
                        double passband2 = Convert.ToDouble(pass2TB.Text);
                        result = SignalProcessing.Sampling(s1, SignalProcessing.SamplingMode.downsampling, 0, M, SignalProcessing.FilterMode.bandstop, passband1, passband2, trans_width, stop_att, sample_freq);
                    }
                }
                else 

                {
                    double passband = Convert.ToDouble(pass2TB.Text);
                    double trans_width = Convert.ToDouble(twTB.Text);
                    double stop_att = Convert.ToDouble(stTB.Text);
                    double sample_freq = Convert.ToDouble(sfTB.Text);
                    int L = Convert.ToInt32(L_TB.Text);
                    int M = Convert.ToInt32(M_TB.Text);
                    if (filterCB.SelectedItem == "Low Pass")
                        result = SignalProcessing.Sampling(s1, SignalProcessing.SamplingMode.upanddownsampling, L, M, SignalProcessing.FilterMode.lowpass, passband, 0, trans_width, stop_att, sample_freq);
                    else if (filterCB.SelectedItem == "High Pass")
                        result = SignalProcessing.Sampling(s1, SignalProcessing.SamplingMode.upanddownsampling, L, M, SignalProcessing.FilterMode.highpass, passband, 0, trans_width, stop_att, sample_freq);
                    else if (filterCB.SelectedItem == "Band Pass")
                    {
                        double passband1 = Convert.ToDouble(pass1TB.Text);
                        double passband2 = Convert.ToDouble(pass2TB.Text);
                        result = SignalProcessing.Sampling(s1, SignalProcessing.SamplingMode.upanddownsampling, L, M, SignalProcessing.FilterMode.bandpass, passband1, passband2, trans_width, stop_att, sample_freq);
                    }
                    else
                    {
                        double passband1 = Convert.ToDouble(pass1TB.Text);
                        double passband2 = Convert.ToDouble(pass2TB.Text);
                        result = SignalProcessing.Sampling(s1, SignalProcessing.SamplingMode.upanddownsampling, L, M, SignalProcessing.FilterMode.bandstop, passband1, passband2, trans_width, stop_att, sample_freq);
                    }
                }
               
            }
          
            DrawSignals.ClearGraph(signal1ZGraph);
            DrawSignals.DrawSignal(result, ref  signal1ZGraph, Color.HotPink, "Result");
            InputReader.SaveSignal(result);
        }

        private void filterCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (filterCB.SelectedItem == "Low Pass" || filterCB.SelectedItem == "High Pass")
            {
                pass1LB.Hide();
                pass1TB.Hide();
                pass2LB.Text = "Passband";
            }
            else
            {
                pass1LB.Show();
                pass1TB.Show();
                pass1LB.Text = "Passband 1";
                pass2LB.Text = "Passband 2";
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Up_sampling_c_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void Up_Down_c_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
