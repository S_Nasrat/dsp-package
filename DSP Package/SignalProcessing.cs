﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;
using System.Diagnostics;
using System.Numerics;


namespace DSP_Package
{
    public class SignalProcessing
    {
        public enum SignalMode { periodic, nonperiodic, none, fastconvolution };
        public enum FilterMode { lowpass, highpass, bandpass, bandstop };
        public enum WindowMode { Rectangular, Hanning, Hamming, Blackman };
        public enum SamplingMode { upsampling, downsampling, upanddownsampling };
        static bool isFolded = false;
        public static Signal AddSignals(Signal s1, Signal s2)
        {
            Signal s3 = new Signal();
            List<double> tempS3y = new List<double>();
            for (int i = 0; i < s2.getYCoords().Count; i++)
            {
                tempS3y.Add(s1.getYCoords()[i] + s2.getYCoords()[i]);
            }
            s3.SetYCoords(tempS3y);
            return s3;
            
        }
        public static List<Interval> QuantizeSignalWithLevels(ref Signal S, int nLevels)
        {
            double deltaIntervals = (S.getYCoords().Max() - S.getYCoords().Min()) / nLevels;
            List<Interval> levels = new List<Interval>();
            levels.Add(new Interval(S.getYCoords().Min(), S.getYCoords().Min() + deltaIntervals,0));
            for (int i = 1; i < nLevels; i++)
            {
                levels.Add(new Interval(levels[i - 1].GetEnd(), levels[i-1].GetEnd() + deltaIntervals,i));
            }
            for (int i = 0; i < S.getYCoords().Count; i++)
            {
                for (int j = 0; j < levels.Count; j++)
                {
                    if (S.getYCoords()[i] >= levels[j].GetStart() && S.getYCoords()[i] <= levels[j].GetEnd())
                    {
                        S.getYCoords()[i] = levels[j].GetMidpoint();
                        break;
                    }
                }
            }
            return levels;
        }
        public static List<Interval> QuantizeSignalWithBits(ref Signal S, int nBits)
        {
            int nLevels = Convert.ToInt32((Math.Pow(2, nBits)));
            double deltaIntervals = (S.getYCoords().Max() - S.getYCoords().Min()) / nLevels;
            List<Interval> levels = new List<Interval>();
            levels.Add(new Interval(S.getYCoords().Min(), S.getYCoords().Min() + deltaIntervals,0));
            for (int i = 1; i < nLevels; i++)
            {
                levels.Add(new Interval(levels[i - 1].GetEnd(), levels[i - 1].GetEnd() + deltaIntervals,i));
                
            }
            for (int i = 0; i < S.getYCoords().Count; i++)
            {
                for (int j = 0; j < levels.Count; j++)
                {
                    if (S.getYCoords()[i] >= levels[j].GetStart() && S.getYCoords()[i] <= levels[j].GetEnd())
                    {
                        S.getYCoords()[i] = levels[j].GetMidpoint();
                        break;
                    }
                }
            }
            
            return levels;
        }
        public static ComplexNumber ConvertExponential(ComplexNumber c)
        {
            
            if (c.GetImaginary() >= 0)
            {
        
                return (new ComplexNumber(Math.Cos(c.GetImaginary()), Math.Sin(c.GetImaginary())));
            }
            else
            {
                
                return (new ComplexNumber(Math.Cos(-c.GetImaginary()), -Math.Sin(-c.GetImaginary())));
            }
        }
        public static void DFT(Signal s, double samplingF,ref ZedGraphControl ampfre,ref ZedGraphControl phasefreq)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            List<ComplexNumber> newYCoords = new List<ComplexNumber>();
            
            int N = s.getYCoords().Count;
            List<double> y = new List<double>(s.getYCoords());

            for (int k = 0; k < N; k++)
            {
                double RealSum = 0;
                double ImaginarySum = 0;
                for (int n = 0; n < N; n++)
                {
                    //double r, i;
                    ComplexNumber temp = new ComplexNumber();
                    temp.SetReal (y[n]);
                    temp.SetImaginary (-((k * 2 * Math.PI * n) / N)); //Remove the "-" = correct :D
                    
                   temp = ConvertExponential(temp);
                    temp.SetReal(y[n] * temp.GetReal());
                    temp.SetImaginary(y[n] * temp.GetImaginary());
                    RealSum += temp.GetReal();
                   ImaginarySum += temp.GetImaginary();
                  


                }
                RealSum = Math.Round(RealSum,13);
                ImaginarySum = Math.Round(ImaginarySum,13);
                newYCoords.Add(new ComplexNumber(RealSum, ImaginarySum));
            }
            timer.Stop();
            double elapsedMs = timer.ElapsedMilliseconds;
            MessageBox.Show("Elapsed Time: "+elapsedMs.ToString());

            List<double> Amp = new List<double>();
            List<double> Phase = new List<double>();
            List<double> Freq = new List<double>();
            for (int i = 0; i < N; i++)
            {
                Amp.Add(Math.Sqrt(Math.Pow(newYCoords[i].GetReal(), 2) + Math.Pow(newYCoords[i].GetImaginary(), 2)));
            }
            for (int i = 0; i < N; i++)
            {
                double temp = newYCoords[i].GetImaginary() / newYCoords[i].GetReal();
                double secondval =  newYCoords[i].GetReal();
                double firstval = newYCoords[i].GetImaginary();
                Phase.Add(Math.Atan2(firstval , secondval));
               
            }
            double delta = (2 * Math.PI) / (N * (1 / samplingF));
            Freq.Add(delta);
            for (int i = 1; i <= N; i++)
            {
                Freq.Add(Freq[i - 1] + delta);
            }
            DrawSignals.DrawSignal(Amp,Freq, ref ampfre, System.Drawing.Color.HotPink, "Amplitude");
            DrawSignals.DrawSignal(Phase, Freq, ref phasefreq, System.Drawing.Color.HotPink, "Phase");
            InputReader.SaveSignal(Amp, Phase);
            List<double> result = new List<double>();
          // result = new List<double>( SignalProcessing.IDFT(newYCoords));

        }
        public static List<double> IDFT(List<ComplexNumber> ReversedS)
        {
            
            List<double> newSignalValues = new List<double>();
            int N = ReversedS.Count;
           // List<double> y = new List<double>(ReversedS.Count);

            for (int k = 0; k < N; k++)
            {
                double RealSum = 0;
                for (int n = 0; n < N; n++)
                {

                    ComplexNumber temp = new ComplexNumber();
                    temp.SetReal(ReversedS[n].GetReal());
                    temp.SetImaginary(((k * 2 * Math.PI * n) / N)); //Remove the "-" = correct :D
                    temp = ConvertExponential(temp);
                    temp.SetReal(ReversedS[n].GetReal() * temp.GetReal());
                    temp.SetImaginary(-(ReversedS[n].GetImaginary() * temp.GetImaginary()));
                    RealSum += temp.GetReal() + temp.GetImaginary();
                    


                }
                RealSum /= ReversedS.Count;
                newSignalValues.Add(RealSum);
            }


            return newSignalValues;
        }
        public static List<ComplexNumber> FFT(List<ComplexNumber> y_list,int n)
        {
            List<ComplexNumber> zeft = new List<ComplexNumber>(y_list);
            if (n == 2)
            {

                ComplexNumber top = new ComplexNumber(y_list[0].GetReal() + y_list[1].GetReal(), 0);
                ComplexNumber bot = new ComplexNumber(y_list[0].GetReal() - y_list[1].GetReal(), 0);
                List<ComplexNumber> myList = new List<ComplexNumber>();
                myList.Add(top);
                myList.Add(bot);
                return myList;

            }
            else
            {
                List<ComplexNumber> L1 = new List<ComplexNumber>();
                List<ComplexNumber> L2 = new List<ComplexNumber>();
                for (int i = 0; i < n; i++)
                {
                    if (i % 2 == 0)
                    {
                        L1.Add(y_list[i]);
                    }
                    else 
                    {
                        L2.Add(y_list[i]);
                    }

                }
                List<ComplexNumber> fft_1 = new List<ComplexNumber>();
                List<ComplexNumber> fft_2 = new List<ComplexNumber>();
                fft_1 = new List<ComplexNumber>(FFT(L1, n / 2));
                fft_2 = new List<ComplexNumber>(FFT(L2, n / 2));

                for (int k = 0; k <= (n / 2) - 1; k++)
                {
                    ComplexNumber W = new ComplexNumber(0,-((2 * Math.PI * k) / n));
                   W = ConvertExponential(W);
                   zeft[k]=(ButterflyTop(fft_1[k], fft_2[k], W));
                   zeft[k+n/2]=(ButterflyBottom(fft_1[k], fft_2[k], W));

                }
                return zeft;

            }
        }
        public static ComplexNumber IButterflyTop(ComplexNumber a, ComplexNumber b, ComplexNumber W)
        {
            double s1 = ((W.GetReal() * b.GetReal()) - (W.GetImaginary() * b.GetImaginary())) + a.GetReal();
            double s2 = ((W.GetReal() * b.GetImaginary()) + (W.GetImaginary() * b.GetReal())) + a.GetImaginary();
            return new ComplexNumber(s1 / 2, s2 / 2);
        }
        public static ComplexNumber IButterflyBottom(ComplexNumber a, ComplexNumber b, ComplexNumber W)
        {
            double s1 = ((W.GetReal() * b.GetReal()) - (W.GetImaginary() * b.GetImaginary())) - a.GetReal();
            double s2 = ((W.GetReal() * b.GetImaginary()) + (W.GetImaginary() * b.GetReal())) - a.GetImaginary();

            return new ComplexNumber(-s1 / 2, -s2 / 2); //-s1 -s2 :P :P :P :P 
        }
        public static ComplexNumber ButterflyTop(ComplexNumber a, ComplexNumber b, ComplexNumber W)
        {
            double s1 =( (W.GetReal() * b.GetReal()) - (W.GetImaginary() * b.GetImaginary())) + a.GetReal() ;
            double s2 =(( W.GetReal() * b.GetImaginary())+ (W.GetImaginary() * b.GetReal()))+a.GetImaginary();
            return new ComplexNumber(s1, s2);
        }
        public static ComplexNumber ButterflyBottom(ComplexNumber a, ComplexNumber b, ComplexNumber W)
        {
            double s1 = ((W.GetReal() * b.GetReal()) - (W.GetImaginary() * b.GetImaginary())) - a.GetReal();
            double s2 = ((W.GetReal() * b.GetImaginary()) +(W.GetImaginary() * b.GetReal())) - a.GetImaginary();

            return new ComplexNumber(-s1, -s2); //-s1 -s2 :P :P :P :P 
        }
        public static List<ComplexNumber> IFFT(List<ComplexNumber> y_list, int n)
        {
            List<ComplexNumber> zeft = new List<ComplexNumber>(y_list);
            if (n == 2)
            {

                ComplexNumber top = new ComplexNumber((y_list[0].GetReal()  + y_list[1].GetReal()) / 2, (y_list[0].GetImaginary()  + y_list[1].GetImaginary()) / 2);
                ComplexNumber bot = new ComplexNumber((y_list[0].GetReal() - y_list[1].GetReal()) / 2, (y_list[0].GetImaginary() - y_list[1].GetImaginary()) / 2);
                List<ComplexNumber> myList = new List<ComplexNumber>();
                myList.Add(top);
                myList.Add(bot);
                return myList;

            }
            else
            {
                List<ComplexNumber> L1 = new List<ComplexNumber>();
                List<ComplexNumber> L2 = new List<ComplexNumber>();
                for (int i = 0; i < n; i++)
                {
                    if (i % 2 == 0)
                    {
                        L1.Add(y_list[i]);
                    }
                    else
                    {
                        L2.Add(y_list[i]);
                    }

                }
                List<ComplexNumber> fft_1 = new List<ComplexNumber>();
                List<ComplexNumber> fft_2 = new List<ComplexNumber>();
                fft_1 = new List<ComplexNumber>(IFFT(L1, n / 2));
                fft_2 = new List<ComplexNumber>(IFFT(L2, n / 2));

                for (int k = 0; k <= (n / 2) - 1; k++)
                {
                    ComplexNumber W = new ComplexNumber(0, ((2 * Math.PI * k) / n));
                    W = ConvertExponential(W);
                    zeft[k] = (IButterflyTop(fft_1[k], fft_2[k], W));
                    //zeft[k].SetReal(zeft[k].GetReal() / n);
                    //zeft[k].SetImaginary(zeft[k].GetImaginary() / n);
                    zeft[k + n / 2] = (IButterflyBottom(fft_1[k], fft_2[k], W));
                    //zeft[k+ n/2].SetReal(zeft[k].GetReal() / n);
                    //zeft[k+ n/2].SetImaginary(zeft[k].GetImaginary() / n);
                    

                }
                //for (int i = 0; i < zeft.Count; i++)
                //{
                //    zeft[i].SetReal(zeft[i].GetReal() / n);
                //    zeft[i].SetImaginary(zeft[i].GetImaginary() / n);

                //}
                    return zeft;

            }
        }
        public static List<ComplexNumber> IFFT_Caller(List<ComplexNumber> l, int size)
        {
            List<ComplexNumber> result = IFFT(l, size);
            //for (int i = 0; i < result.Count; i++)
            //{
            //    result[i].SetReal(result[i].GetReal() / size);
            //    result[i].SetImaginary(result[i].GetImaginary() / size);

            //}
            return result;
            
        }

        public static Signal MultiplySignals(Signal s1, Signal s2)
        {
            Signal s3 = new Signal();
            List<double> tempS3y = new List<double>();
            for (int i = 0; i < s2.getYCoords().Count; i++)
            {
                tempS3y.Add(s1.getYCoords()[i] * s2.getYCoords()[i]);
            }
            s3.SetYCoords(tempS3y);
            return s3;

        }
        public static Signal MultiplySignalByConst(Signal s1, double c)
        {
            Signal s3 = new Signal();
            List<double> tempS3y = new List<double>();
            for (int i = 0; i < s1.getYCoords().Count; i++)
            {
                tempS3y.Add(s1.getYCoords()[i] *c);
            }
            s3.SetYCoords(tempS3y);
            return s3;

        }
        public static Signal FoldSignal(Signal s)
        {
            Signal newS = new Signal(s.getXCoords(),s.getYCoords());
            newS.getYCoords().Reverse();
            List<double> xs = new List<double>(newS.getXCoords());
            for (int i = 0; i < newS.getXCoords().Count; i++)
            {
                xs[i] *= -1;
            }
            xs.Reverse();
            newS.SetXCoords(xs);
                isFolded = true;
            return newS;
        }
        public static Signal ShiftSignal(Signal s, int shiftamount)
        {
            Signal newS = new Signal();
            newS.SetXCoords(s.getXCoords());
            newS.SetYCoords(s.getYCoords());
            if (isFolded)
            {
                if (shiftamount > 0)
                {
                    List<double> xs = new List<double>(s.getXCoords());
                    for (int i = 0; i < newS.getXCoords().Count; i++)
                    {
                        xs[i] -= Math.Abs(shiftamount);
                    }
                    newS.SetXCoords(xs);
                }
                else
                {
                    List<double> xs = new List<double>(s.getXCoords());
                    for (int i = 0; i < newS.getXCoords().Count; i++)
                    {
                        xs[i] += Math.Abs(shiftamount);
                    }
                    newS.SetXCoords(xs);
                }
            }
            else //Signal is not folded (n) 
            {
                if (shiftamount > 0)
                {
                    List<double> xs = new List<double>(s.getXCoords());
                    for (int i = 0; i < newS.getXCoords().Count; i++)
                    {
                        xs[i] -= Math.Abs(shiftamount);
                    }
                    newS.SetXCoords(xs);
                }
                else
                {
                    List<double> xs = new List<double>(s.getXCoords());
                    for (int i = 0; i < newS.getXCoords().Count; i++)
                    {
                        xs[i] += Math.Abs(shiftamount);
                    }
                    newS.SetXCoords(xs);
                }
            }
            return newS;
        }
        public static Signal ConvolutionSum(Signal x, Signal h)
        {
            Signal y = new Signal();
            List<double> x_n = new List<double>(x.getYCoords());
            List<double> h_n = new List<double>(h.getYCoords());
            List<double> ysum = new List<double>();
            int num_of_terms = (x.getYCoords().Count + h.getYCoords().Count)-1;
            for (int n = 0; n < num_of_terms; n++)
            {
                double tempsum = 0;
                for (int k = 0; k <= n; k++)
                {
                    if (k >= x_n.Count || n - k >= h_n.Count)
                        tempsum += 0;
                    else
                    tempsum += x_n[k] * h_n[n - k];
                }
                ysum.Add(tempsum);
            }
            y.SetYCoords(ysum);
            return y;
        }
        public static Signal DirectCorrelation(Signal x1, Signal x2,SignalMode type)
        {               
            
            List<double> r_n = new List<double>();
            List<double> x1_y = new List<double>(x1.getYCoords());
            List<double> x2_y = new List<double>(x2.getYCoords());
          
            int num_of_points = x1_y.Count;
            if (x1_y.Count != x2_y.Count)
            {
                num_of_points = x1_y.Count + x2_y.Count - 1;
                for (int i = x1_y.Count; i < num_of_points; i++)
                {
                    x1_y.Add(0);
                }
                for (int i = x2_y.Count; i < num_of_points; i++)
                {
                    x2_y.Add(0);
                }
            }
            List<double> Normalize_x1 = new List<double>(x1_y);
            List<double> Normalize_x2 = new List<double>(x2_y);

            for (int j = 0; j < x1_y.Count; j++)
            {
                double tempSum = 0;
                for (int n = 0; n < x1_y.Count; n++)
                {
                    tempSum += x1_y[n] * x2_y[n];
                }
                r_n.Add(tempSum / x1_y.Count);
                if (type == SignalMode.periodic)
                    x2_y = RotateLeft(x2_y, 1);
                else
                    x2_y = ShiftRight(x2_y, 1);
                   

            }
            return NormalizeSignal(Normalize_x1, Normalize_x2, r_n) ;

           

        }
        public static Signal FastCorrelation(Signal x1, Signal x2, SignalMode type)
        {
            List<double> r_n = new List<double>();
            List<double> x1_y = new List<double>(x1.getYCoords());
            List<double> x2_y = new List<double>(x2.getYCoords());

            int num_of_points = x1_y.Count;
            if (x1_y.Count != x2_y.Count)
            {
                num_of_points = x1_y.Count + x2_y.Count - 1;
                for (int i = x1_y.Count; i < num_of_points; i++)
                {
                    x1_y.Add(0);
                }
                for (int i = x2_y.Count; i < num_of_points; i++)
                {
                    x2_y.Add(0);
                }
            }
            //Get FFT for each
            List<ComplexNumber> x1_c = new List<ComplexNumber>();
            List<ComplexNumber> x2_c = new List<ComplexNumber>();
            List<ComplexNumber> x_c = new List<ComplexNumber>();
            for (int i = 0; i < x1_y.Count; i++)
            {
                x1_c.Add(new ComplexNumber(x1_y[i], 0));
            }
            for (int i = 0; i < x2_y.Count; i++)
            {
                x2_c.Add(new ComplexNumber(x2_y[i], 0));
            }
            x1_c = FFT(x1_c, x1_c.Count);
            x2_c = FFT(x2_c, x2_c.Count);
            //Get Conjugate of X1
            if (type != SignalMode.fastconvolution)
            {
                for (int i = 0; i < x1_c.Count; i++)
                {
                    x1_c[i].SetImaginary(x1_c[i].GetImaginary() * -1);
                }
            }
            x_c = MultiplyTwoComplexList(x1_c, x2_c);
            x_c = IFFT_Caller(x_c, x_c.Count);  
            for (int i = 0; i < x1_y.Count; i++)
            {
                r_n.Add(x_c[i].GetReal());
            }
            if(type != SignalMode.fastconvolution)
            for (int i = 0; i < x1_y.Count; i++)
            {
                r_n[i] /= x1_y.Count;
            }
            return NormalizeSignal(x1_y, x2_y, r_n);

        }
        public static Signal NormalizeSignal(List<double> x1,List<double> x2,List<double> resultant)
        {
            List<double> r_n = new List<double>(resultant);
            List<double> x1_y = new List<double>(x1);
            List<double> x2_y = new List<double>(x2);
            double sumX1 = 0, sumX2 = 0, denominator = 0;
            for (int i = 0; i < x1_y.Count; i++)
            {
              //  sumX1 += Math.Pow(2, x1_y[i]);
                sumX1 += x1_y[i] * x1_y[i];
            }
            for (int i = 0; i < x2_y.Count; i++)
            {
                //sumX2 += Math.Pow(2, x2_y[i]);
                sumX2 += x2_y[i] * x2_y[i];
            }
            denominator = Math.Sqrt((sumX1 * sumX2)) / x1_y.Count;
            for (int i = 0; i < r_n.Count; i++)
            {
                r_n[i] /= denominator;
            }
            return new Signal(r_n);
        }
        public static Signal Filter(Signal s,FilterMode filt, double passband_edge_freq_start,double passband_edge_freq_end,double transition_width,double stopband_att,double sampling_freq)
        {
           // List<double> s_Y = new List<double>(s.getYCoords());
            List<double> h_delta = new List<double>();
            List<double> window_function = new List<double>();
        
            List<double> HxWindow = new List<double>();
            double delta_F = transition_width / sampling_freq;
            WindowMode mWindow = 0;
            double N = 0;
           //Window Method
            if (stopband_att >= 0 && stopband_att <= 21) //Rectangular
            {
                //Transition Width Normalized
                N = Math.Round(0.9 / delta_F);
                mWindow = WindowMode.Rectangular;

            }
            else if (stopband_att >= 22 && stopband_att <= 44) //Hanning
            {
                N = Math.Round(3.1 / delta_F);
                mWindow = WindowMode.Hanning;
            }
            else if (stopband_att >= 45 && stopband_att <= 53) //Hamming
            {
                N = Math.Round(3.3 / delta_F);
                mWindow = WindowMode.Hamming;
            }
            else if (stopband_att >= 55 && stopband_att < 74) //Blackman
            {
                N = Math.Round(5.5 / delta_F);
                mWindow = WindowMode.Blackman;
            }
            //if N is even, N = N -1 
            if (N % 2 == 0)
                N--;
            double f_dash_1=0, f_dash_2=0;
            for (int i = 0; i < N/2; i++)
            {
                //If i == 0 base case for H(n)
                if (i == 0)
                {
                    switch (filt)
                    {
                        case FilterMode.lowpass:
                            f_dash_1 = (passband_edge_freq_start + transition_width / 2) / sampling_freq;
                            h_delta.Add(2 * f_dash_1);
                            break;
                        case FilterMode.highpass:
                            f_dash_1 = (passband_edge_freq_start + transition_width / 2) / sampling_freq;
                            h_delta.Add(1 - 2 * f_dash_1);
                            break;
                        case FilterMode.bandpass:
                            f_dash_1 = (passband_edge_freq_start -transition_width / 2) / sampling_freq;
                            f_dash_2 = (passband_edge_freq_end + transition_width / 2) / sampling_freq;
                            h_delta.Add(2 * (f_dash_2 - f_dash_1));
                            break;
                        case FilterMode.bandstop:
                            f_dash_1 = (passband_edge_freq_start + transition_width / 2) / sampling_freq;
                            f_dash_2 = (passband_edge_freq_end - transition_width / 2) / sampling_freq;
                            h_delta.Add(2 * (f_dash_2 + f_dash_1));
                            break;

                    }
                    switch (mWindow)
                    {
                        case WindowMode.Rectangular:
                            window_function.Add(1);
                            break;
                        case WindowMode.Hanning:
                            window_function.Add(0.5+0.5*Math.Cos((2*Math.PI*i)/(N)));
                            break;
                        case WindowMode.Hamming:
                            window_function.Add(0.54 + 0.46 * Math.Cos((2 * Math.PI * i) / (N)));
                            break;
                        case WindowMode.Blackman:
                            window_function.Add(0.42 + 0.5 * Math.Cos((2 * Math.PI * i) / (N-1))+0.08*Math.Cos((4*Math.PI*i)/(N-1)));
                            break;

                    }
                    HxWindow.Add(h_delta[0] * window_function[0]);
                }
                else
                {
                    //Calculate Window
                    switch (mWindow)
                    {
                        case WindowMode.Rectangular:
                            window_function.Add(1);
                            break;
                        case WindowMode.Hanning:
                            window_function.Add(0.5 + 0.5 * Math.Cos((2 * Math.PI * i) / (N)));
                            break;
                        case WindowMode.Hamming:
                            window_function.Add(0.54 + 0.46 * Math.Cos((2 * Math.PI * i) / (N)));
                            break;
                        case WindowMode.Blackman:
                            window_function.Add(0.42 + 0.5 * Math.Cos((2 * Math.PI * i) / (N - 1)) + 0.08 * Math.Cos((4 * Math.PI * i) / (N - 1)));
                            break;

                    }
                    //For i !=0  Calculate H
                    switch (filt)
                    {
                        case FilterMode.lowpass:

                            h_delta.Add(((2 * f_dash_1) / (2 * Math.PI * i * f_dash_1)) * (Math.Sin(i * 2 * Math.PI * f_dash_1)));
                            break;
                        case FilterMode.highpass:

                            h_delta.Add(((-2 * f_dash_1) / (2 * Math.PI * i * f_dash_1)) * (Math.Sin(2 * Math.PI * i * f_dash_1)));
                            break;
                        case FilterMode.bandpass:
                           double h1_delta = (((2 * f_dash_1) / (2 * Math.PI * i * f_dash_1)) * (Math.Sin(2 * Math.PI * i * f_dash_1)));
                           double h2_delta = (((2 * f_dash_2) / (2 * Math.PI * i * f_dash_2)) * (Math.Sin(2 * Math.PI * i * f_dash_2)));
                            h_delta.Add(h2_delta-h1_delta);
                            break;
                        case FilterMode.bandstop:
                             h1_delta = (((2 * f_dash_1) / (2 * Math.PI * i * f_dash_1)) * (Math.Sin(2 * Math.PI * i * f_dash_1)));
                             h2_delta = (((2 * f_dash_2) / (2 * Math.PI * i * f_dash_2)) * (Math.Sin(2 * Math.PI * i * f_dash_2)));
                            h_delta.Add(1-(h2_delta-h1_delta));
                            break;
                    }
                    HxWindow.Add(h_delta[i] * window_function[i]);
                }
               
            }
            InputReader.SaveSignal(HxWindow);
            //Complete HxWindow 
            //List<double> full_Signal = new List<double>();
            //double temp_0 = HxWindow[0];
            //HxWindow.RemoveAt(0);
            //HxWindow.Reverse();
            //full_Signal.AddRange(HxWindow);
            //full_Signal.Add(temp_0);
            //HxWindow.Reverse();
            //full_Signal.AddRange(HxWindow);


            //Compute Fast Convo
            Signal result = new Signal();
            result = ConvolutionSum(s, new Signal(HxWindow));
            return result;
        }
        public static Signal Sampling(Signal s,SamplingMode s_mode,int L,int M, FilterMode filt, double passband_edge_freq_start, double passband_edge_freq_end, double transition_width, double stopband_att, double sampling_freq)
        {
            List<double> s_Ys = new List<double>(s.getYCoords());
            List<double> s_YsUp = new List<double>();
            Signal result = new Signal();

            if (s_mode == SamplingMode.upsampling)
            {
                for (int i = 0; i < s_Ys.Count; i ++) //Add element of S,followed by L zeroes
                {
                    s_YsUp.Add(s_Ys[i]);
                    for (int j = 0; j < L-1; j++)
                    {
                        s_YsUp.Add(0);
                    }
                }
                //Filter
                result = Filter(s, filt, passband_edge_freq_start, passband_edge_freq_end, transition_width, stopband_att, sampling_freq);
                

            }
            else if (s_mode == SamplingMode.downsampling)
            {
                //Filter 
                result = Filter(s, filt, passband_edge_freq_start, passband_edge_freq_end, transition_width, stopband_att, sampling_freq);
                List<double> r_Y = new List<double>(result.getYCoords());
                List<double> r_Ydown = new List<double>();
                //Down Sampling
                for (int i = 0; i < r_Y.Count; i += M)
                {
                    r_Ydown.Add(r_Y[i]);
                }
                result.SetYCoords(r_Ydown);
               
            }
            else
            {
                //Up Sampling
                for (int i = 0; i < s_Ys.Count; i ++)
                {
                    s_YsUp.Add(s_Ys[i]);
                    for (int j = 0; j < L-1; j++)
                    {
                        s_YsUp.Add(0);
                    }
                }
                //Adjust Signals Y Coords 
                s.SetYCoords(s_YsUp);
                //Filter 
                
               result =  Filter(s, filt, passband_edge_freq_start, passband_edge_freq_end, transition_width, stopband_att, sampling_freq);
                List<double> r_Y = new List<double>(result.getYCoords());
                List<double> r_Ydown = new List<double>();
                //Down Sampling
                for (int i = 0; i < r_Y.Count; i+= M)
                {
                    r_Ydown.Add(r_Y[i]) ;
                }
                result.SetYCoords(r_Ydown);
            }
            return result;   
        }
        //Helper Functions
        private static List<double> ShiftRight(List<double> x2_y, int p)
        {
            for (int i = x2_y.Count - 1; i > 1; i--)
            {
                x2_y[i] = x2_y[i - 1];
            }
            x2_y[0] = 0;
            return x2_y;
        }
        public static List<double> RotateLeft(List<double> l, int shiftamount)
        {
            //double temp = l[0];
            //for (int i = shiftamount; i < l.Count; i++)
            //{
            //    l[i - shiftamount] = l[i];
            //}
            //l[l.Count - 1] = temp;
            double temp = l[0];
            l.RemoveAt(0);
            l.Add(temp);
            return l;
        }
        public static List<ComplexNumber> MultiplyTwoComplexList(List<ComplexNumber> l1, List<ComplexNumber> l2)
        {
            List<ComplexNumber> result = new List<ComplexNumber>();
            for (int i = 0; i < l1.Count; i++)
            {
                double realpart = (l1[i].GetReal() * l2[i].GetReal()) + ((l1[i].GetImaginary() * l2[i].GetImaginary()) * -1);
                double imaginarypart = (l1[i].GetReal()*l2[i].GetImaginary())+(l2[i].GetReal()*l1[i].GetImaginary());
                result.Add(new ComplexNumber(realpart, imaginarypart));
            }
            return result;
        }
        //Helper Classes
        public class Interval
        {
            double start;
            double end;
            double midpoint;
            string code;
            public Interval(double s, double e,int index)
            {
                this.start = s;
                this.end = e;
               // start = Math.Round(start, 2);
              //  end = Math.Round(end, 2);
                code = Convert.ToString(index, 2);
                CalculateMidpoint();
            }
            public void SetStart(double s)
            {
                this.start = s;
             //  start = Math.Round(start, 2);
            }
            public void SetEnd(double e)
            {
                this.end = e;
               //end = Math.Round(end, 2);
            }
            public double GetStart()
            {
                return start;
            }
            public double GetEnd()
            {

                return end;
            }
            public void CalculateMidpoint()
            {
                midpoint = (end + start) / 2;
                //Rounding to nearest 2 decimal places if needed:
              //  midpoint = Math.Round(midpoint, 2);
            }
            public double GetMidpoint()
            {
                return midpoint;
            }
            public void SetCode(int n)
            {
                code = Convert.ToString(n, 2);
            }
            public string GetCode()
            {
                return code;
            }
        }

    }
}
