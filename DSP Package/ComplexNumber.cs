﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSP_Package
{
    public class ComplexNumber
    {
        double real;
        double imaginary;
        double phase;
        double amp;
        public ComplexNumber(double x,double y)
        {
            this.real = x;
            this.imaginary = y;
            amp = Math.Sqrt(Math.Pow(real,2)+Math.Pow(imaginary,2));
            phase = Math.Atan2(imaginary,real);
        }
       
        public ComplexNumber()
        {

        }
        public void SetReal(double r)
        {
            this.real = r;
        }
        public void SetImaginary(double i)
        {
            this.imaginary = i;
        }
        public double GetReal()
        {
            return real;
        }
        public double GetImaginary()
        {
            return imaginary;
        }
        public void UpdatePolar()
        {
            amp = Math.Sqrt(Math.Pow(real, 2) + Math.Pow(imaginary, 2));
            phase = Math.Atan2(imaginary, real);
        }
        public void PolarToComplex()
        {
            real = amp * Math.Cos(phase);
            imaginary = amp * Math.Sin(phase);
        }
        public void SetAmp(double a)
        {
            amp = a;
        }
        public double GetAmp()
        {
            return amp;
        }
        public void SetPhase(double p)
        {
            phase = p;
        }
        public double GetPhase()
        {
            return phase;
        }
       

    }
}
