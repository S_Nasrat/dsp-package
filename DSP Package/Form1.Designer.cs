﻿namespace DSP_Package
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.browseTB = new System.Windows.Forms.Button();
            this.pathTB = new System.Windows.Forms.TextBox();
            this.modeGB = new System.Windows.Forms.GroupBox();
            this.twosignalsRB = new System.Windows.Forms.RadioButton();
            this.onesignalRB = new System.Windows.Forms.RadioButton();
            this.t = new System.ComponentModel.BackgroundWorker();
            this.quantizeBT = new System.Windows.Forms.Button();
            this.quantmodeGB = new System.Windows.Forms.GroupBox();
            this.quantmodeTB = new System.Windows.Forms.TextBox();
            this.levelsmodeRB = new System.Windows.Forms.RadioButton();
            this.bitsmodeRB = new System.Windows.Forms.RadioButton();
            this.signal1ZGraph = new ZedGraph.ZedGraphControl();
            this.ampfreqZGraph = new ZedGraph.ZedGraphControl();
            this.phasefreqZGraph = new ZedGraph.ZedGraphControl();
            this.idftBT = new System.Windows.Forms.Button();
            this.applydftCB = new System.Windows.Forms.CheckBox();
            this.samplefTB = new System.Windows.Forms.TextBox();
            this.samplefLB = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.shiftBT = new System.Windows.Forms.Button();
            this.shftTB = new System.Windows.Forms.TextBox();
            this.foldBT = new System.Windows.Forms.Button();
            this.multiplyBT = new System.Windows.Forms.Button();
            this.multiplyconstBT = new System.Windows.Forms.Button();
            this.multiplyconstTB = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.convBT = new System.Windows.Forms.Button();
            this.directcorrelationBT = new System.Windows.Forms.Button();
            this.fftRB = new System.Windows.Forms.RadioButton();
            this.directRB = new System.Windows.Forms.RadioButton();
            this.button3 = new System.Windows.Forms.Button();
            this.crossDirectRB = new System.Windows.Forms.RadioButton();
            this.crossFFTRB = new System.Windows.Forms.RadioButton();
            this.periodicCB = new System.Windows.Forms.CheckBox();
            this.nonperiodicCB = new System.Windows.Forms.CheckBox();
            this.convGB = new System.Windows.Forms.GroupBox();
            this.normalconvRB = new System.Windows.Forms.RadioButton();
            this.fastconvRB = new System.Windows.Forms.RadioButton();
            this.filterapplyTB = new System.Windows.Forms.Button();
            this.filterCB = new System.Windows.Forms.ComboBox();
            this.pass1TB = new System.Windows.Forms.TextBox();
            this.pass2TB = new System.Windows.Forms.TextBox();
            this.twTB = new System.Windows.Forms.TextBox();
            this.stTB = new System.Windows.Forms.TextBox();
            this.pass1LB = new System.Windows.Forms.Label();
            this.sfTB = new System.Windows.Forms.TextBox();
            this.twLB = new System.Windows.Forms.Label();
            this.pass2LB = new System.Windows.Forms.Label();
            this.stLB = new System.Windows.Forms.Label();
            this.sfLB = new System.Windows.Forms.Label();
            this.L_TB = new System.Windows.Forms.TextBox();
            this.M_TB = new System.Windows.Forms.TextBox();
            this.L = new System.Windows.Forms.Label();
            this.M = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Up_Down_CB = new System.Windows.Forms.CheckBox();
            this.Down_sampling_CB = new System.Windows.Forms.CheckBox();
            this.Up_sampling_CB = new System.Windows.Forms.CheckBox();
            this.noSampingCB = new System.Windows.Forms.CheckBox();
            this.modeGB.SuspendLayout();
            this.quantmodeGB.SuspendLayout();
            this.convGB.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // browseTB
            // 
            this.browseTB.Location = new System.Drawing.Point(107, 25);
            this.browseTB.Name = "browseTB";
            this.browseTB.Size = new System.Drawing.Size(75, 23);
            this.browseTB.TabIndex = 0;
            this.browseTB.Text = "Browse";
            this.browseTB.UseVisualStyleBackColor = true;
            this.browseTB.Click += new System.EventHandler(this.browseTB_Click);
            // 
            // pathTB
            // 
            this.pathTB.Location = new System.Drawing.Point(188, 27);
            this.pathTB.Name = "pathTB";
            this.pathTB.Size = new System.Drawing.Size(263, 20);
            this.pathTB.TabIndex = 1;
            // 
            // modeGB
            // 
            this.modeGB.Controls.Add(this.twosignalsRB);
            this.modeGB.Controls.Add(this.onesignalRB);
            this.modeGB.Location = new System.Drawing.Point(12, 12);
            this.modeGB.Name = "modeGB";
            this.modeGB.Size = new System.Drawing.Size(89, 72);
            this.modeGB.TabIndex = 2;
            this.modeGB.TabStop = false;
            this.modeGB.Text = "Signal Mode:";
            // 
            // twosignalsRB
            // 
            this.twosignalsRB.AutoSize = true;
            this.twosignalsRB.Location = new System.Drawing.Point(6, 42);
            this.twosignalsRB.Name = "twosignalsRB";
            this.twosignalsRB.Size = new System.Drawing.Size(68, 17);
            this.twosignalsRB.TabIndex = 4;
            this.twosignalsRB.TabStop = true;
            this.twosignalsRB.Text = "2 Signals";
            this.twosignalsRB.UseVisualStyleBackColor = true;
            // 
            // onesignalRB
            // 
            this.onesignalRB.AutoSize = true;
            this.onesignalRB.Location = new System.Drawing.Point(6, 19);
            this.onesignalRB.Name = "onesignalRB";
            this.onesignalRB.Size = new System.Drawing.Size(63, 17);
            this.onesignalRB.TabIndex = 3;
            this.onesignalRB.TabStop = true;
            this.onesignalRB.Text = "1 Signal";
            this.onesignalRB.UseVisualStyleBackColor = true;
            // 
            // quantizeBT
            // 
            this.quantizeBT.Location = new System.Drawing.Point(107, 61);
            this.quantizeBT.Name = "quantizeBT";
            this.quantizeBT.Size = new System.Drawing.Size(83, 23);
            this.quantizeBT.TabIndex = 5;
            this.quantizeBT.Text = "Quantize";
            this.quantizeBT.UseVisualStyleBackColor = true;
            this.quantizeBT.Click += new System.EventHandler(this.quantizeBT_Click);
            // 
            // quantmodeGB
            // 
            this.quantmodeGB.Controls.Add(this.quantmodeTB);
            this.quantmodeGB.Controls.Add(this.levelsmodeRB);
            this.quantmodeGB.Controls.Add(this.bitsmodeRB);
            this.quantmodeGB.Location = new System.Drawing.Point(925, 0);
            this.quantmodeGB.Name = "quantmodeGB";
            this.quantmodeGB.Size = new System.Drawing.Size(200, 90);
            this.quantmodeGB.TabIndex = 7;
            this.quantmodeGB.TabStop = false;
            this.quantmodeGB.Text = "Quanization Mode:";
            // 
            // quantmodeTB
            // 
            this.quantmodeTB.Location = new System.Drawing.Point(100, 36);
            this.quantmodeTB.Name = "quantmodeTB";
            this.quantmodeTB.Size = new System.Drawing.Size(100, 20);
            this.quantmodeTB.TabIndex = 2;
            // 
            // levelsmodeRB
            // 
            this.levelsmodeRB.AutoSize = true;
            this.levelsmodeRB.Location = new System.Drawing.Point(7, 59);
            this.levelsmodeRB.Name = "levelsmodeRB";
            this.levelsmodeRB.Size = new System.Drawing.Size(86, 17);
            this.levelsmodeRB.TabIndex = 1;
            this.levelsmodeRB.TabStop = true;
            this.levelsmodeRB.Text = "Using Levels";
            this.levelsmodeRB.UseVisualStyleBackColor = true;
            // 
            // bitsmodeRB
            // 
            this.bitsmodeRB.AutoSize = true;
            this.bitsmodeRB.Location = new System.Drawing.Point(7, 20);
            this.bitsmodeRB.Name = "bitsmodeRB";
            this.bitsmodeRB.Size = new System.Drawing.Size(72, 17);
            this.bitsmodeRB.TabIndex = 0;
            this.bitsmodeRB.TabStop = true;
            this.bitsmodeRB.Text = "Using Bits";
            this.bitsmodeRB.UseVisualStyleBackColor = true;
            // 
            // signal1ZGraph
            // 
            this.signal1ZGraph.Location = new System.Drawing.Point(12, 90);
            this.signal1ZGraph.Name = "signal1ZGraph";
            this.signal1ZGraph.ScrollGrace = 0D;
            this.signal1ZGraph.ScrollMaxX = 0D;
            this.signal1ZGraph.ScrollMaxY = 0D;
            this.signal1ZGraph.ScrollMaxY2 = 0D;
            this.signal1ZGraph.ScrollMinX = 0D;
            this.signal1ZGraph.ScrollMinY = 0D;
            this.signal1ZGraph.ScrollMinY2 = 0D;
            this.signal1ZGraph.Size = new System.Drawing.Size(900, 730);
            this.signal1ZGraph.TabIndex = 8;
            this.signal1ZGraph.UseExtendedPrintDialog = true;
            this.signal1ZGraph.Load += new System.EventHandler(this.signal1ZGraph_Load);
            // 
            // ampfreqZGraph
            // 
            this.ampfreqZGraph.Location = new System.Drawing.Point(925, 94);
            this.ampfreqZGraph.Name = "ampfreqZGraph";
            this.ampfreqZGraph.ScrollGrace = 0D;
            this.ampfreqZGraph.ScrollMaxX = 0D;
            this.ampfreqZGraph.ScrollMaxY = 0D;
            this.ampfreqZGraph.ScrollMaxY2 = 0D;
            this.ampfreqZGraph.ScrollMinX = 0D;
            this.ampfreqZGraph.ScrollMinY = 0D;
            this.ampfreqZGraph.ScrollMinY2 = 0D;
            this.ampfreqZGraph.Size = new System.Drawing.Size(553, 360);
            this.ampfreqZGraph.TabIndex = 10;
            this.ampfreqZGraph.UseExtendedPrintDialog = true;
            // 
            // phasefreqZGraph
            // 
            this.phasefreqZGraph.Location = new System.Drawing.Point(925, 460);
            this.phasefreqZGraph.Name = "phasefreqZGraph";
            this.phasefreqZGraph.ScrollGrace = 0D;
            this.phasefreqZGraph.ScrollMaxX = 0D;
            this.phasefreqZGraph.ScrollMaxY = 0D;
            this.phasefreqZGraph.ScrollMaxY2 = 0D;
            this.phasefreqZGraph.ScrollMinX = 0D;
            this.phasefreqZGraph.ScrollMinY = 0D;
            this.phasefreqZGraph.ScrollMinY2 = 0D;
            this.phasefreqZGraph.Size = new System.Drawing.Size(553, 360);
            this.phasefreqZGraph.TabIndex = 11;
            this.phasefreqZGraph.UseExtendedPrintDialog = true;
            // 
            // idftBT
            // 
            this.idftBT.Location = new System.Drawing.Point(196, 61);
            this.idftBT.Name = "idftBT";
            this.idftBT.Size = new System.Drawing.Size(75, 23);
            this.idftBT.TabIndex = 12;
            this.idftBT.Text = "Apply IDFT";
            this.idftBT.UseVisualStyleBackColor = true;
            this.idftBT.Click += new System.EventHandler(this.idftBT_Click);
            // 
            // applydftCB
            // 
            this.applydftCB.AutoSize = true;
            this.applydftCB.Location = new System.Drawing.Point(731, 12);
            this.applydftCB.Name = "applydftCB";
            this.applydftCB.Size = new System.Drawing.Size(76, 17);
            this.applydftCB.TabIndex = 13;
            this.applydftCB.Text = "Apply DFT";
            this.applydftCB.UseVisualStyleBackColor = true;
            // 
            // samplefTB
            // 
            this.samplefTB.Location = new System.Drawing.Point(731, 35);
            this.samplefTB.Name = "samplefTB";
            this.samplefTB.Size = new System.Drawing.Size(100, 20);
            this.samplefTB.TabIndex = 14;
            // 
            // samplefLB
            // 
            this.samplefLB.AutoSize = true;
            this.samplefLB.Location = new System.Drawing.Point(622, 38);
            this.samplefLB.Name = "samplefLB";
            this.samplefLB.Size = new System.Drawing.Size(103, 13);
            this.samplefLB.TabIndex = 15;
            this.samplefLB.Text = "Sampling Frequency";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(277, 61);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "Apply FFT";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // shiftBT
            // 
            this.shiftBT.Location = new System.Drawing.Point(650, 61);
            this.shiftBT.Name = "shiftBT";
            this.shiftBT.Size = new System.Drawing.Size(75, 23);
            this.shiftBT.TabIndex = 18;
            this.shiftBT.Text = "Shift Signal";
            this.shiftBT.UseVisualStyleBackColor = true;
            this.shiftBT.Click += new System.EventHandler(this.shiftBT_Click);
            // 
            // shftTB
            // 
            this.shftTB.Location = new System.Drawing.Point(731, 63);
            this.shftTB.Name = "shftTB";
            this.shftTB.Size = new System.Drawing.Size(43, 20);
            this.shftTB.TabIndex = 19;
            // 
            // foldBT
            // 
            this.foldBT.Location = new System.Drawing.Point(569, 61);
            this.foldBT.Name = "foldBT";
            this.foldBT.Size = new System.Drawing.Size(75, 23);
            this.foldBT.TabIndex = 17;
            this.foldBT.Text = "Fold Signal";
            this.foldBT.UseVisualStyleBackColor = true;
            this.foldBT.Click += new System.EventHandler(this.foldBT_Click_1);
            // 
            // multiplyBT
            // 
            this.multiplyBT.Location = new System.Drawing.Point(358, 61);
            this.multiplyBT.Name = "multiplyBT";
            this.multiplyBT.Size = new System.Drawing.Size(75, 23);
            this.multiplyBT.TabIndex = 20;
            this.multiplyBT.Text = "Multiply";
            this.multiplyBT.UseVisualStyleBackColor = true;
            this.multiplyBT.Click += new System.EventHandler(this.multiplyBT_Click);
            // 
            // multiplyconstBT
            // 
            this.multiplyconstBT.Location = new System.Drawing.Point(439, 54);
            this.multiplyconstBT.Name = "multiplyconstBT";
            this.multiplyconstBT.Size = new System.Drawing.Size(75, 36);
            this.multiplyconstBT.TabIndex = 21;
            this.multiplyconstBT.Text = "Multiply by Constant";
            this.multiplyconstBT.UseVisualStyleBackColor = true;
            this.multiplyconstBT.Click += new System.EventHandler(this.multiplyconstBT_Click);
            // 
            // multiplyconstTB
            // 
            this.multiplyconstTB.Location = new System.Drawing.Point(520, 63);
            this.multiplyconstTB.Name = "multiplyconstTB";
            this.multiplyconstTB.Size = new System.Drawing.Size(43, 20);
            this.multiplyconstTB.TabIndex = 22;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(457, 27);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(134, 23);
            this.button2.TabIndex = 23;
            this.button2.Text = "Load XY Signal";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // convBT
            // 
            this.convBT.Location = new System.Drawing.Point(837, 61);
            this.convBT.Name = "convBT";
            this.convBT.Size = new System.Drawing.Size(75, 23);
            this.convBT.TabIndex = 24;
            this.convBT.Text = "Convolve";
            this.convBT.UseVisualStyleBackColor = true;
            this.convBT.Click += new System.EventHandler(this.convBT_Click);
            // 
            // directcorrelationBT
            // 
            this.directcorrelationBT.Location = new System.Drawing.Point(1131, 52);
            this.directcorrelationBT.Name = "directcorrelationBT";
            this.directcorrelationBT.Size = new System.Drawing.Size(75, 40);
            this.directcorrelationBT.TabIndex = 25;
            this.directcorrelationBT.Text = "Direct Correlation";
            this.directcorrelationBT.UseVisualStyleBackColor = true;
            this.directcorrelationBT.Click += new System.EventHandler(this.directcorrelationBT_Click);
            // 
            // fftRB
            // 
            this.fftRB.AutoSize = true;
            this.fftRB.Location = new System.Drawing.Point(1131, 12);
            this.fftRB.Name = "fftRB";
            this.fftRB.Size = new System.Drawing.Size(44, 17);
            this.fftRB.TabIndex = 26;
            this.fftRB.TabStop = true;
            this.fftRB.Text = "FFT";
            this.fftRB.UseVisualStyleBackColor = true;
            // 
            // directRB
            // 
            this.directRB.AutoSize = true;
            this.directRB.Location = new System.Drawing.Point(1131, 31);
            this.directRB.Name = "directRB";
            this.directRB.Size = new System.Drawing.Size(53, 17);
            this.directRB.TabIndex = 27;
            this.directRB.TabStop = true;
            this.directRB.Text = "Direct";
            this.directRB.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(1212, 52);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 40);
            this.button3.TabIndex = 28;
            this.button3.Text = "Cross Correlation";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // crossDirectRB
            // 
            this.crossDirectRB.AutoSize = true;
            this.crossDirectRB.Location = new System.Drawing.Point(1212, 31);
            this.crossDirectRB.Name = "crossDirectRB";
            this.crossDirectRB.Size = new System.Drawing.Size(53, 17);
            this.crossDirectRB.TabIndex = 30;
            this.crossDirectRB.TabStop = true;
            this.crossDirectRB.Text = "Direct";
            this.crossDirectRB.UseVisualStyleBackColor = true;
            // 
            // crossFFTRB
            // 
            this.crossFFTRB.AutoSize = true;
            this.crossFFTRB.Location = new System.Drawing.Point(1212, 12);
            this.crossFFTRB.Name = "crossFFTRB";
            this.crossFFTRB.Size = new System.Drawing.Size(44, 17);
            this.crossFFTRB.TabIndex = 29;
            this.crossFFTRB.TabStop = true;
            this.crossFFTRB.Text = "FFT";
            this.crossFFTRB.UseVisualStyleBackColor = true;
            // 
            // periodicCB
            // 
            this.periodicCB.AutoSize = true;
            this.periodicCB.Location = new System.Drawing.Point(1301, 12);
            this.periodicCB.Name = "periodicCB";
            this.periodicCB.Size = new System.Drawing.Size(64, 17);
            this.periodicCB.TabIndex = 31;
            this.periodicCB.Text = "Periodic";
            this.periodicCB.UseVisualStyleBackColor = true;
            // 
            // nonperiodicCB
            // 
            this.nonperiodicCB.AutoSize = true;
            this.nonperiodicCB.Location = new System.Drawing.Point(1301, 32);
            this.nonperiodicCB.Name = "nonperiodicCB";
            this.nonperiodicCB.Size = new System.Drawing.Size(86, 17);
            this.nonperiodicCB.TabIndex = 32;
            this.nonperiodicCB.Text = "Non-periodic";
            this.nonperiodicCB.UseVisualStyleBackColor = true;
            // 
            // convGB
            // 
            this.convGB.Controls.Add(this.normalconvRB);
            this.convGB.Controls.Add(this.fastconvRB);
            this.convGB.Location = new System.Drawing.Point(838, 0);
            this.convGB.Name = "convGB";
            this.convGB.Size = new System.Drawing.Size(81, 57);
            this.convGB.TabIndex = 33;
            this.convGB.TabStop = false;
            this.convGB.Text = "Convolution";
            // 
            // normalconvRB
            // 
            this.normalconvRB.AutoSize = true;
            this.normalconvRB.Location = new System.Drawing.Point(7, 36);
            this.normalconvRB.Name = "normalconvRB";
            this.normalconvRB.Size = new System.Drawing.Size(58, 17);
            this.normalconvRB.TabIndex = 1;
            this.normalconvRB.TabStop = true;
            this.normalconvRB.Text = "Normal";
            this.normalconvRB.UseVisualStyleBackColor = true;
            // 
            // fastconvRB
            // 
            this.fastconvRB.AutoSize = true;
            this.fastconvRB.Location = new System.Drawing.Point(7, 20);
            this.fastconvRB.Name = "fastconvRB";
            this.fastconvRB.Size = new System.Drawing.Size(45, 17);
            this.fastconvRB.TabIndex = 0;
            this.fastconvRB.TabStop = true;
            this.fastconvRB.Text = "Fast";
            this.fastconvRB.UseVisualStyleBackColor = true;
            // 
            // filterapplyTB
            // 
            this.filterapplyTB.Location = new System.Drawing.Point(1403, 61);
            this.filterapplyTB.Name = "filterapplyTB";
            this.filterapplyTB.Size = new System.Drawing.Size(75, 23);
            this.filterapplyTB.TabIndex = 34;
            this.filterapplyTB.Text = "Apply Filter";
            this.filterapplyTB.UseVisualStyleBackColor = true;
            this.filterapplyTB.Click += new System.EventHandler(this.button4_Click);
            // 
            // filterCB
            // 
            this.filterCB.FormattingEnabled = true;
            this.filterCB.Items.AddRange(new object[] {
            "Low Pass",
            "High Pass",
            "Band Pass",
            "Band Stop"});
            this.filterCB.Location = new System.Drawing.Point(1403, 32);
            this.filterCB.Name = "filterCB";
            this.filterCB.Size = new System.Drawing.Size(75, 21);
            this.filterCB.TabIndex = 35;
            this.filterCB.SelectedIndexChanged += new System.EventHandler(this.filterCB_SelectedIndexChanged);
            // 
            // pass1TB
            // 
            this.pass1TB.Location = new System.Drawing.Point(1543, 32);
            this.pass1TB.Name = "pass1TB";
            this.pass1TB.Size = new System.Drawing.Size(51, 20);
            this.pass1TB.TabIndex = 36;
            // 
            // pass2TB
            // 
            this.pass2TB.Location = new System.Drawing.Point(1543, 58);
            this.pass2TB.Name = "pass2TB";
            this.pass2TB.Size = new System.Drawing.Size(51, 20);
            this.pass2TB.TabIndex = 37;
            // 
            // twTB
            // 
            this.twTB.Location = new System.Drawing.Point(1543, 84);
            this.twTB.Name = "twTB";
            this.twTB.Size = new System.Drawing.Size(51, 20);
            this.twTB.TabIndex = 38;
            // 
            // stTB
            // 
            this.stTB.Location = new System.Drawing.Point(1543, 110);
            this.stTB.Name = "stTB";
            this.stTB.Size = new System.Drawing.Size(51, 20);
            this.stTB.TabIndex = 39;
            // 
            // pass1LB
            // 
            this.pass1LB.AutoSize = true;
            this.pass1LB.Location = new System.Drawing.Point(1484, 36);
            this.pass1LB.Name = "pass1LB";
            this.pass1LB.Size = new System.Drawing.Size(54, 13);
            this.pass1LB.TabIndex = 40;
            this.pass1LB.Text = "Passband";
            // 
            // sfTB
            // 
            this.sfTB.Location = new System.Drawing.Point(1543, 136);
            this.sfTB.Name = "sfTB";
            this.sfTB.Size = new System.Drawing.Size(51, 20);
            this.sfTB.TabIndex = 41;
            // 
            // twLB
            // 
            this.twLB.AutoSize = true;
            this.twLB.Location = new System.Drawing.Point(1486, 87);
            this.twLB.Name = "twLB";
            this.twLB.Size = new System.Drawing.Size(51, 13);
            this.twLB.TabIndex = 42;
            this.twLB.Text = "Tran Wid";
            // 
            // pass2LB
            // 
            this.pass2LB.AutoSize = true;
            this.pass2LB.Location = new System.Drawing.Point(1486, 61);
            this.pass2LB.Name = "pass2LB";
            this.pass2LB.Size = new System.Drawing.Size(54, 13);
            this.pass2LB.TabIndex = 43;
            this.pass2LB.Text = "Passband";
            // 
            // stLB
            // 
            this.stLB.AutoSize = true;
            this.stLB.Location = new System.Drawing.Point(1489, 113);
            this.stLB.Name = "stLB";
            this.stLB.Size = new System.Drawing.Size(45, 13);
            this.stLB.TabIndex = 44;
            this.stLB.Text = "Stop Att";
            // 
            // sfLB
            // 
            this.sfLB.AutoSize = true;
            this.sfLB.Location = new System.Drawing.Point(1482, 139);
            this.sfLB.Name = "sfLB";
            this.sfLB.Size = new System.Drawing.Size(58, 13);
            this.sfLB.TabIndex = 45;
            this.sfLB.Text = "Samp Freq";
            // 
            // L_TB
            // 
            this.L_TB.Location = new System.Drawing.Point(1539, 300);
            this.L_TB.Name = "L_TB";
            this.L_TB.Size = new System.Drawing.Size(51, 20);
            this.L_TB.TabIndex = 46;
            // 
            // M_TB
            // 
            this.M_TB.Location = new System.Drawing.Point(1539, 336);
            this.M_TB.Name = "M_TB";
            this.M_TB.Size = new System.Drawing.Size(51, 20);
            this.M_TB.TabIndex = 47;
            // 
            // L
            // 
            this.L.AutoSize = true;
            this.L.Location = new System.Drawing.Point(1518, 307);
            this.L.Name = "L";
            this.L.Size = new System.Drawing.Size(13, 13);
            this.L.TabIndex = 48;
            this.L.Text = "L";
            // 
            // M
            // 
            this.M.AutoSize = true;
            this.M.Location = new System.Drawing.Point(1518, 339);
            this.M.Name = "M";
            this.M.Size = new System.Drawing.Size(16, 13);
            this.M.TabIndex = 49;
            this.M.Text = "M";
            this.M.Click += new System.EventHandler(this.label2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.noSampingCB);
            this.groupBox1.Controls.Add(this.Up_Down_CB);
            this.groupBox1.Controls.Add(this.Down_sampling_CB);
            this.groupBox1.Controls.Add(this.Up_sampling_CB);
            this.groupBox1.Location = new System.Drawing.Point(1487, 173);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(103, 121);
            this.groupBox1.TabIndex = 50;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sampling";
            // 
            // Up_Down_CB
            // 
            this.Up_Down_CB.AutoSize = true;
            this.Up_Down_CB.Location = new System.Drawing.Point(17, 65);
            this.Up_Down_CB.Name = "Up_Down_CB";
            this.Up_Down_CB.Size = new System.Drawing.Size(48, 17);
            this.Up_Down_CB.TabIndex = 2;
            this.Up_Down_CB.Text = "Both";
            this.Up_Down_CB.UseVisualStyleBackColor = true;
            this.Up_Down_CB.CheckedChanged += new System.EventHandler(this.Up_Down_c_CheckedChanged);
            // 
            // Down_sampling_CB
            // 
            this.Down_sampling_CB.AutoSize = true;
            this.Down_sampling_CB.Location = new System.Drawing.Point(17, 42);
            this.Down_sampling_CB.Name = "Down_sampling_CB";
            this.Down_sampling_CB.Size = new System.Drawing.Size(97, 17);
            this.Down_sampling_CB.TabIndex = 1;
            this.Down_sampling_CB.Text = "DownSampling";
            this.Down_sampling_CB.UseVisualStyleBackColor = true;
            // 
            // Up_sampling_CB
            // 
            this.Up_sampling_CB.AutoSize = true;
            this.Up_sampling_CB.Location = new System.Drawing.Point(17, 19);
            this.Up_sampling_CB.Name = "Up_sampling_CB";
            this.Up_sampling_CB.Size = new System.Drawing.Size(83, 17);
            this.Up_sampling_CB.TabIndex = 0;
            this.Up_sampling_CB.Text = "UpSampling";
            this.Up_sampling_CB.UseVisualStyleBackColor = true;
            this.Up_sampling_CB.CheckedChanged += new System.EventHandler(this.Up_sampling_c_CheckedChanged);
            // 
            // noSampingCB
            // 
            this.noSampingCB.AutoSize = true;
            this.noSampingCB.Location = new System.Drawing.Point(17, 88);
            this.noSampingCB.Name = "noSampingCB";
            this.noSampingCB.Size = new System.Drawing.Size(86, 17);
            this.noSampingCB.TabIndex = 3;
            this.noSampingCB.Text = "No Sampling";
            this.noSampingCB.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1596, 828);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.M);
            this.Controls.Add(this.L);
            this.Controls.Add(this.M_TB);
            this.Controls.Add(this.L_TB);
            this.Controls.Add(this.sfLB);
            this.Controls.Add(this.stLB);
            this.Controls.Add(this.pass2LB);
            this.Controls.Add(this.twLB);
            this.Controls.Add(this.sfTB);
            this.Controls.Add(this.pass1LB);
            this.Controls.Add(this.stTB);
            this.Controls.Add(this.twTB);
            this.Controls.Add(this.pass2TB);
            this.Controls.Add(this.pass1TB);
            this.Controls.Add(this.filterCB);
            this.Controls.Add(this.filterapplyTB);
            this.Controls.Add(this.convGB);
            this.Controls.Add(this.nonperiodicCB);
            this.Controls.Add(this.periodicCB);
            this.Controls.Add(this.crossDirectRB);
            this.Controls.Add(this.crossFFTRB);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.directRB);
            this.Controls.Add(this.fftRB);
            this.Controls.Add(this.directcorrelationBT);
            this.Controls.Add(this.convBT);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.multiplyconstTB);
            this.Controls.Add(this.multiplyconstBT);
            this.Controls.Add(this.multiplyBT);
            this.Controls.Add(this.shftTB);
            this.Controls.Add(this.shiftBT);
            this.Controls.Add(this.foldBT);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.samplefLB);
            this.Controls.Add(this.samplefTB);
            this.Controls.Add(this.applydftCB);
            this.Controls.Add(this.idftBT);
            this.Controls.Add(this.phasefreqZGraph);
            this.Controls.Add(this.ampfreqZGraph);
            this.Controls.Add(this.signal1ZGraph);
            this.Controls.Add(this.quantmodeGB);
            this.Controls.Add(this.quantizeBT);
            this.Controls.Add(this.modeGB);
            this.Controls.Add(this.pathTB);
            this.Controls.Add(this.browseTB);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.modeGB.ResumeLayout(false);
            this.modeGB.PerformLayout();
            this.quantmodeGB.ResumeLayout(false);
            this.quantmodeGB.PerformLayout();
            this.convGB.ResumeLayout(false);
            this.convGB.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button browseTB;
        private System.Windows.Forms.TextBox pathTB;
        private System.Windows.Forms.GroupBox modeGB;
        private System.Windows.Forms.RadioButton twosignalsRB;
        private System.Windows.Forms.RadioButton onesignalRB;
        private System.ComponentModel.BackgroundWorker t;
        private System.Windows.Forms.Button quantizeBT;
        private System.Windows.Forms.GroupBox quantmodeGB;
        private System.Windows.Forms.TextBox quantmodeTB;
        private System.Windows.Forms.RadioButton levelsmodeRB;
        private System.Windows.Forms.RadioButton bitsmodeRB;
        private ZedGraph.ZedGraphControl signal1ZGraph;
        private ZedGraph.ZedGraphControl ampfreqZGraph;
        private ZedGraph.ZedGraphControl phasefreqZGraph;
        private System.Windows.Forms.Button idftBT;
        private System.Windows.Forms.CheckBox applydftCB;
        private System.Windows.Forms.TextBox samplefTB;
        private System.Windows.Forms.Label samplefLB;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button shiftBT;
        private System.Windows.Forms.TextBox shftTB;
        private System.Windows.Forms.Button foldBT;
        private System.Windows.Forms.Button multiplyBT;
        private System.Windows.Forms.Button multiplyconstBT;
        private System.Windows.Forms.TextBox multiplyconstTB;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button convBT;
        private System.Windows.Forms.Button directcorrelationBT;
        private System.Windows.Forms.RadioButton fftRB;
        private System.Windows.Forms.RadioButton directRB;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.RadioButton crossDirectRB;
        private System.Windows.Forms.RadioButton crossFFTRB;
        private System.Windows.Forms.CheckBox periodicCB;
        private System.Windows.Forms.CheckBox nonperiodicCB;
        private System.Windows.Forms.GroupBox convGB;
        private System.Windows.Forms.RadioButton normalconvRB;
        private System.Windows.Forms.RadioButton fastconvRB;
        private System.Windows.Forms.Button filterapplyTB;
        private System.Windows.Forms.ComboBox filterCB;
        private System.Windows.Forms.TextBox pass1TB;
        private System.Windows.Forms.TextBox pass2TB;
        private System.Windows.Forms.TextBox twTB;
        private System.Windows.Forms.TextBox stTB;
        private System.Windows.Forms.Label pass1LB;
        private System.Windows.Forms.TextBox sfTB;
        private System.Windows.Forms.Label twLB;
        private System.Windows.Forms.Label pass2LB;
        private System.Windows.Forms.Label stLB;
        private System.Windows.Forms.Label sfLB;
        private System.Windows.Forms.TextBox L_TB;
        private System.Windows.Forms.TextBox M_TB;
        private System.Windows.Forms.Label L;
        private System.Windows.Forms.Label M;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox Up_Down_CB;
        private System.Windows.Forms.CheckBox Down_sampling_CB;
        private System.Windows.Forms.CheckBox Up_sampling_CB;
        private System.Windows.Forms.CheckBox noSampingCB;
    }
}

