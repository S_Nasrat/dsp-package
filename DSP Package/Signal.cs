﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSP_Package
{
    public class Signal
    {
        InputReader reader = new InputReader();
        List<double> yCoords = new List<double>();
        List<double> xCoords = new List<double>();

        public Signal(List<double> L)
        {
            this.yCoords = new List<double>(L);
        }
        public Signal(List<double> x,List<double> y)
        {
            this.xCoords = new List<double>(x);
            this.yCoords = new List<double>(y);
        }
        public Signal() { }
        public void SetSignal(string filePath)
        {
            yCoords = new List<double>(InputReader.readSignal(filePath));
           
        }
        public void SetYCoords(List<double> y)
        {
            this.yCoords = new List<double>(y);
        }
        public void SetXCoords(List<double> x)
        {
            this.xCoords = new List<double>(x);
        }
        public List<double> getYCoords()
        {
            return this.yCoords;
        }
        public List<double> getXCoords()
        {
            return this.xCoords;
        }
    }
}
